package com.sample.chatapp4

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.NetworkInfo
import android.net.NetworkInfo.DetailedState
import android.net.wifi.WifiManager
import android.os.Parcelable
import android.util.Log
import android.widget.Toast

class NetworkReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        //네트워크 상태 값 받아오기
        if (WifiManager.NETWORK_STATE_CHANGED_ACTION == intent!!.action) {
            val info =
                intent!!.getParcelableExtra<Parcelable>(WifiManager.EXTRA_NETWORK_INFO) as NetworkInfo?
            val state = info!!.detailedState
            if (state == DetailedState.CONNECTED) { //네트워크 연결 상태
                Log.d("test", "connected!")
            } else if (state == DetailedState.DISCONNECTED) { //네트워크 연결 해제
                Log.d("test", "disconnected! :(")
                Toast.makeText(context,"인터넷 연결이 필요합니다",Toast.LENGTH_SHORT).show()
            }
        }
    }
}