package com.sample.chatapp4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.Toolbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.sample.chatapp4.ModelClasses.Users
import com.sample.chatapp4.second.MessageChatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_firend_profile.*
import kotlinx.android.synthetic.main.activity_message_chat.*
import kotlinx.android.synthetic.main.list_friend_item.view.*

class FirendProfileActivity : AppCompatActivity() {
    var userIdVisit : String = ""  // 상대방(프로필 클릭시)
    var firebaseUser : FirebaseUser?= null
    var reference : DatabaseReference? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firend_profile)

        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbar_firend)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            finish()
        }

        intent = intent
        userIdVisit = intent.getStringExtra("visit_id")!! // 상대방 id
        firebaseUser = FirebaseAuth.getInstance().currentUser // 내 정보 가져옴(로그인한 사용자)

        btn_chat.setOnClickListener {
            val intent = Intent(this, MessageChatActivity::class.java)
            intent.putExtra("visit_id",userIdVisit) // 클릭한 친구 id값 넘김
            this?.startActivity(intent)
            finish()
        }

        reference = FirebaseDatabase.getInstance("https://messengerapp-45874-default-rtdb.firebaseio.com/").reference
            .child("Users").child(userIdVisit)
        reference!!.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                val user: Users? = p0.getValue(Users::class.java)
                friend_name.text = user?.username
                Picasso.get().load(user?.profile).resize(150,150).into(profile_pic)

            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }
}