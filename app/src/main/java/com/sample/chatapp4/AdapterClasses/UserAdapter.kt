package com.sample.chatapp4.AdapterClasses

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.sample.chatapp4.FirendProfileActivity
import com.sample.chatapp4.ModelClasses.Chat
import com.sample.chatapp4.ModelClasses.Chatlist
import com.sample.chatapp4.ModelClasses.Users
import com.sample.chatapp4.R
import com.sample.chatapp4.second.MessageChatActivity
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.list_friend_item.view.*

class UserAdapter(
    mContext: Context,
    mUsers: ArrayList<Users>,
    isChatCheck : Boolean
) : RecyclerView.Adapter<UserAdapter.ViewHolder?>()
{
    private val mContext:Context
    private val mUsers : List<Users>
    private var isChatCheck: Boolean
    private  var usersChatList = ArrayList<String>()

    init{
        this.mUsers= mUsers
        this.mContext =mContext
        this.isChatCheck = isChatCheck
        Log.d("adapter","초기화")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.list_friend_item,parent,false)

        Log.d("adapter","onCreateViewHolder")
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        Log.d("adapter","getItemCount")
        return mUsers.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user: Users = mUsers[position]
        val ref = FirebaseDatabase.getInstance("https://messengerapp-45874-default-rtdb.firebaseio.com/").reference.child("ChatList")
            .child(mUsers[position].uid!!)

        ref!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                usersChatList.clear()

                for(dataSnapshot in p0.children)
                {
                    val chatlist = dataSnapshot.getValue(Chatlist::class.java)
//                    Log.d("test",chatlist?.getId().toString())
//                    usersChatList.add(chatlist?.getId().toString())
                    usersChatList.add(chatlist?.getId().toString())
//                    arrayList.add(chatlist.getId().toString())
                }

            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })

        val reference = FirebaseDatabase.getInstance("https://messengerapp-45874-default-rtdb.firebaseio.com/")
            .reference.child("Chats")
        reference.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(p0: DataSnapshot) {
                (usersChatList as ArrayList<Chat>).clear()
                for(snapshot in p0.children){
                    val chat = snapshot.getValue(Chat::class.java)
                    if(chat!!.sender == user.uid || chat!!.receiver == user.uid){
                        holder.lastChat.text = chat.message.toString()
//                        Log.d("test",chat.toString())
                    }

                }
            }

            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }
        })


        holder.userNameTxt.text=  user!!.username
        Picasso.get().load(user.profile).placeholder(R.drawable.profile_img).into(holder.profileImageView)
//        holder.userNameTxt.text=  "친구이름넣는곳"
        holder.lastChat.text = "${position}"

       // Picasso.get().load(user.getProfile()).placeholder(R.drawable.profile_img).into(holder.profileImageView)


        holder.itemView.setOnClickListener {
            val options = arrayOf<CharSequence>(
                "메세지 보내기",
                "프로필 보기"
            )
            val builder : AlertDialog.Builder = AlertDialog.Builder(mContext)
            builder.setTitle("What do you want?")
            builder.setItems(options,DialogInterface.OnClickListener{dialog, position ->
                if(position == 0){
                    val intent = Intent(mContext,MessageChatActivity::class.java)
                    intent.putExtra("visit_id",user.uid) // 클릭한 친구 id값 넘김
                    mContext?.startActivity(intent)
                }
                if(position == 1){
                    val intent = Intent(mContext, FirendProfileActivity::class.java)
                    intent.putExtra("visit_id",user.uid) // 클릭한 친구 id값 넘김
                    mContext?.startActivity(intent)

                }
            })

            builder.show()
        }

    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
    {

        var userNameTxt: TextView
        var profileImageView : CircleImageView
//        var onlineImageView: CircleImageView
//        var offlineImageView: CircleImageView
        var lastChat : TextView
        // var lastMessageTxt: TextView   // 최근 메세지 내용 <- 메세지 리스트 adapter에 추가

        init{
            userNameTxt = itemView.findViewById(R.id.username)
            profileImageView = itemView.findViewById(R.id.profile_image)
//            onlineImageView = itemView.findViewById(R.id.image_online)
//            offlineImageView = itemView.findViewById(R.id.image_offline)
            lastChat = itemView.findViewById(R.id.lastchat)
            //    lastMessageTxt = itemView.findViewById(R.id.message_last)
        }
    } // end of class ViewHolder

}// end of class UserAdpater










