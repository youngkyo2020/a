package com.sample.chatapp4

import android.Manifest
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.sample.chatapp4.Network.RetrofitBuilder
import com.sample.chatapp4.Network.RoomsData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Splash : AppCompatActivity() {
    private val SPLASH_VIEW_TIME: Long = 2000
    private lateinit var mAuth: FirebaseAuth
    val CLIENT_ID = "seeart"
    val CLIENT_SECRET = "seeart1234"
    var token = ""
    var id = ""
    var password = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mAuth = FirebaseAuth.getInstance()
        val sharedPreference = getSharedPreferences("token", MODE_PRIVATE)
        val editor = sharedPreference.edit()
        token = sharedPreference.getString("token","")!!
        id = sharedPreference.getString("id","")!!
        password = sharedPreference.getString("password","")!!
        if (id != "" && password != ""){
            //자동 로그인
            mAuth.signInWithEmailAndPassword(id,password).addOnCompleteListener { task->
                if(task.isSuccessful)
                {
                    val intent = Intent(this@Splash,MainActivity::class.java)
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }else
                {
                    Toast.makeText(this@Splash,"로그인 실패 : "+task.exception!!.message.toString(),
                        Toast.LENGTH_LONG).show()
                }
            }
        }

        var input = HashMap<String, String>()
        input["mb_id"] = CLIENT_ID
        input["password"] =CLIENT_SECRET
        RetrofitBuilder.api.post(input).enqueue(object: Callback<RoomsData> {
            override fun onResponse(call: Call<RoomsData>, response: Response<RoomsData>) {
                if(response.isSuccessful) {
//                    Log.d("test", "연결성공")
                    var data: RoomsData = response.body()!!
                    token = data.access_token
//                    Log.d("test","받은 토큰 : "+token)
                    editor.putString("token", token)
                    editor.apply()
//                    Log.d("test", a.access_token)
                }
            }

            override fun onFailure(call: Call<RoomsData>, t: Throwable) {
                Log.d("test", "연결실패")
            }

        })



        //네트워크 상태 확인 브로드캐스트 리시버 등록
        val filter = IntentFilter()
        val receiver = NetworkReceiver()
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
        registerReceiver(receiver, filter)


        //권한 체크
        TedPermission.with(this)
            .setPermissionListener(permissionListener)
            .setDeniedMessage("접근 거부하셨습니다.\n[설정] - [권한]에서 권한을 허용해주세요.")
            .setPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .check()

    }

    val permissionListener: PermissionListener = object : PermissionListener {
        override fun onPermissionGranted() {
            Log.d("test","권한 ㅇㅇ")
            // 2초 뒤 login activity로 전환
            Handler().postDelayed({
                val intent = Intent(this@Splash,LoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            },SPLASH_VIEW_TIME)
        }

        override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
            Log.d("test","권한 거부됨")
        }
    }

    override fun onBackPressed() {
    }

}