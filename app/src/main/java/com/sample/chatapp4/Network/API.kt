package com.sample.chatapp4.Network

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface API {
    // x-www-urlencoded
    @FormUrlEncoded // x-www-urlencoded로 보내겠다.
    @POST("/api/login")
    fun post(@FieldMap param: HashMap<String, String>): Call<RoomsData> // FieldMap을 사용해 여러개의 변수를 보내겠다 선언

}